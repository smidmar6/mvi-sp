# mvi-sp
####################################
# Modelování časových řad 
####################################

## Zadání

Úkolem projektu je modelovat dvě dostatečně velké časové řady 
ze serveru datamarket.com (např. teploty a srážky) pomocí neuronových sítí,
konkrétně CNN, RNN a LSTM. Datasety se rozdělí do tří částí (trénovací,
validační a testovací), aby bylo možné vyhodnotit kvalitu modelu 
na pro něj neznámých datech.


## Data

Přiložené datasety byly (bez hlavičky) staženy ve formátu .csv 
ze serveru https://datamarket.com.


## Spuštění

Přiložený zdrojový soubor ve formátu .ipynb ve webovém prohlížeči
programem Jupyter.
Potřebné jsou zejména knihovny TensorFlow (backend, verze 1.5.0)
a Keras (verze 2.2.4).

